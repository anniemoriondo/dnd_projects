let currentRoom;

class DungeonRoom {
  constructor(roomName) {
    this.roomName = roomName;
    this.nextDoor = {
      north : undefined,
      northeast : undefined,
      east : undefined,
      southeast : undefined,
      south : undefined,
      southwest : undefined,
      west : undefined,
      northwest : undefined,
      up : undefined,
      down : undefined,
    }
    this.roomText = '';
  }

  /*I need a good way to do this.
  isAdjacent(room){
    let adjacentRooms = Object.keys(this.nextDoor).map(function (key){
      return this.nextDoor[key.toString()].roomName();
    });
    if(adjacentRooms.includes(room.roomName)){
      return true;
    } else{
      return false;
    }
  }*/


  go(direction){
    let roomToGo = this.nextDoor[direction];
    if(roomToGo){
      currentRoom = roomToGo;
      } else {
      console.log ('You can\'t go there.');
    }
  }


}

const greatHall = new DungeonRoom('Great Hall');
const throneRoom = new DungeonRoom('Throne Room');

greatHall.nextDoor.north = throneRoom;
throneRoom.nextDoor.south = greatHall;

currentRoom = greatHall;
console.log(currentRoom.roomName);
currentRoom.go('north');
console.log(currentRoom.roomName);
currentRoom.go('east');
//console.log(currentRoom.isAdjacent(throneRoom));
