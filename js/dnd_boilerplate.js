//Find an ability mod given an ability score.
function findAbilityMod(rawScore){
	return Math.floor((rawScore - 10)/2);
}

//Roll an n-sided die.
function dN(n){
	return Math.floor(Math.random() * n) + 1;
}

//Make an ability check.
function abilityCheck(abilityMod){
	let dieRoll = dN(20);
	let isNat20;
	if (dieRoll === 20){
			isNat20 = true;
	} else {
		isNat20 = false;
	}
	return [dieRoll + abilityMod, isNat20];
}

//Player character class.
class PlayerCharacter{
	constructor (pcArray){
	//constructor(pcName, pcRace, pcClasz, pcStats){
		this.pcName = pcArray[0];
		this.pcRace = pcArray[1];
		this.pcClasz = pcArray[2];
		///this._pcLevel = pcLevel; //we will deal with level later
		this.pcStats = pcArray[3];
		//Stats go in order: STR DEX CON INT WIS CHA
		this.strScore = this.pcStats[0];
		this.dexScore = this.pcStats[1];
		this.conScore = this.pcStats[2];
		this.intScore = this.pcStats[3];
		this.wisScore = this.pcStats[4];
		this.chaScore = this.pcStats[5];

		//Calculate the PC's ability mods.
		this.str = findAbilityMod(this.strScore);
		this.dex = findAbilityMod(this.dexScore);
		this.con = findAbilityMod(this.conScore);
		this.int = findAbilityMod(this.intScore);
		this.wis = findAbilityMod(this.wisScore);
		this.cha = findAbilityMod(this.chaScore);
		}
}


//Class constructor will push each playable race to this array
let playableRaces = [];

// Class for a playable race
class PlayableRace{
	constructor(name, subtypes){
		this.name = name;
		this.subtypes = subtypes;
		playableRaces.push(this);
	}
}

//Class for a type within a playable race
class PlayableSubtype {
	constructor(name, parentRace, ability_increases){ //we will add special functions in a bit
//how will this even work - need a function that "cleverly uses your ASIs"
		this.name = name;
		this.parentRace = parentRace;
		this.ability_increases = ability_increases;
		//this.asi_function = asi_function;
		this.parentRace.subtypes.push(this);
	}

	apply_ASIs(raw_scores, your_clasz){
		let adjusted_scores = [];
		for (let x = 0; x < 6; x++){
			adjusted_scores.push(raw_scores[x] + this.ability_increases[x]);
		}

		if (this.parentRace === halfelf){
			//This is Antal's way of applying half elf bonuses.
			let applicable_optimizer = your_clasz.optimizer
			const fixedIncrease      = this.ability_increases.indexOf(2);
			const increasePriorities = fixedIncrease === applicable_optimizer.indexOf(5) ? [4,3]
			                         : fixedIncrease === applicable_optimizer.indexOf(4) ? [5,3]
			                         :                                                     [5,4];

			for (const p of increasePriorities)
			  ++adjusted_scores[applicable_optimizer.indexOf(p)]
		}
		return adjusted_scores;
	}

}

//Dwarves.
const dwarf = new PlayableRace('Dwarf', []);
const hill_dwarf = new PlayableSubtype('Hill Dwarf', dwarf, [0, 0, 2, 0, 1, 0] );
const mountain_dwarf = new PlayableSubtype('Mountain Dwarf', dwarf, [2, 0, 2, 0, 0, 0]);

//Elves.
const elf = new PlayableRace('Elf', []);
const high_elf = new PlayableSubtype('High Elf', elf, [0, 2, 0, 1, 0, 0]);
const wood_elf = new PlayableSubtype('Wood Elf', elf, [0, 2, 0, 0, 1, 0]);
const drow = new PlayableSubtype('Drow', elf, [0, 2, 0, 0, 0, 1]);

//Halflings.
const halfling = new PlayableRace('Halfling', []);
const lightfoot_halfling = new PlayableSubtype('Lightfoot Halfling', halfling, [0, 2, 0, 0, 0, 1]);
const stout_halfling = new PlayableSubtype('Stout Halfling', halfling, [0, 2, 1, 0, 0, 0]);

//Humans.
const human = new PlayableRace('Human', []);
const general_human = new PlayableSubtype('Human', human, [1, 1, 1, 1, 1, 1]);
///should I bother with variant humans? aaaagh

//Dragonborn.
const dragonborn = new PlayableRace('Dragonborn', []);
const general_dragonborn = new PlayableSubtype('Dragonborn', dragonborn, [2, 0, 0, 0, 0, 1]);

//Gnomes.
const gnome = new PlayableRace('Gnome', []);
const forest_gnome = new PlayableSubtype('Forest Gnome', gnome, [0, 1, 0, 2, 0, 0]);
const rock_gnome = new PlayableSubtype('Rock Gnome', gnome, [0, 0, 1, 2, 0, 0]);

//Half-elves.
const halfelf = new PlayableRace('Half-Elf', []);
const general_halfelf = new PlayableSubtype('Half-Elf', halfelf, [0, 0, 0, 0, 0, 2]);
//two "chosen" ability scores are selected according to clasz.
//See PlayableSubtype constructor.

//Half-orcs.
const halforc = new PlayableRace('Half-Orc', []);
const general_halforc = new PlayableSubtype('Half-Orc', halforc, [2, 0, 1, 0, 0, 0]);

//Tieflings.
const tiefling = new PlayableRace('Tiefling', []);
const general_tiefling = new PlayableSubtype('Tiefling', tiefling, [0, 0, 0, 1, 0, 2]);

//Merfolk.
const merfolk = new PlayableRace('Merfolk', []);
const general_merfolk = new PlayableSubtype('Merfolk', merfolk, [2, 0, 0, 0, 0, 1]);


//Class constructor will push each playable class ('clasz') to this array
let playableClaszes = [];

//Class for a playable rpg class ("clasz" for clarity)
class PlayableClasz{
	constructor(name, archetypes, optimizer){
		this.name = name;
		this.archetypes = archetypes;
		this.optimizer = optimizer;
		playableClaszes.push(this);
	}
}

//Class for a playable archetype
class PlayableArchetype{
	constructor(name, parentClass){
//I'm not sure when we're going to deal with archetypes
		this.name = name;
		this.parentClass = parentClass;
		this.parentClass.archetypes.push(this);
	}
}

		//Stats go in order: STR DEX CON INT WIS CHA
const barbarian = new PlayableClasz('Barbarian', [], [5, 3, 4, 0, 2, 1]);
const bard = new PlayableClasz('Bard', [], [1, 4, 3, 0, 2, 5]);
const cleric = new PlayableClasz('Cleric', [], [2, 0, 4, 1, 5, 3]);
const druid = new PlayableClasz('Druid', [], [1, 0, 4, 3, 5, 2]);
const fighter = new PlayableClasz('Fighter', [], [5, 3, 4, 2, 1, 0]);
const monk = new PlayableClasz('Monk', [], [3, 5, 2, 0, 4, 1]);
const paladin = new PlayableClasz('Paladin', [], [5, 0, 2, 1, 3, 4]);
const ranger = new PlayableClasz('Ranger', [], [3, 5, 2, 1, 4, 0]);
const rogue = new PlayableClasz('Rogue', [], [0, 5, 2, 3, 1, 4]);
const sorcerer = new PlayableClasz('Sorcerer', [], [0, 1, 4, 3, 2, 5]);
const warlock = new PlayableClasz('Warlock', [], [1, 2, 4, 3, 0, 5]);
const wizard = new PlayableClasz('Wizard', [], [0, 2, 4, 5, 3, 1]);


//Max is ready to help us with debugging when needed.
//const maxArray = ['Max', human, sorcerer, [9, 13, 11, 16, 15, 14]];
//const max = new PlayerCharacter (maxArray);

//Exporting for use in the character generator.
module.exports.dN = dN;
module.exports.abilityCheck = abilityCheck;
module.exports.playableRaces = playableRaces;
module.exports.playableClaszes = playableClaszes;
module.exports.PlayerCharacter = PlayerCharacter;
