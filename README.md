[try it in browser](https://ttrpg.anniemoriondo.com/) | [gitlab repo](https://gitlab.com/anniemoriondo/dnd_projects)

This is a character generator for TTRPGs! It's currently set up for D&D 5e.

Here's how the character generator logic works:
* A random character Class is rolled (e.g. Fighter, Bard, or Sorcerer.)
* A random character Race is rolled (e.g. Elf, Half-Orc, or Human.)
* NEW December 2018: A random character Alignment is rolled (thank you [Parmalier](https://twitter.com/audacityXrose) for the suggestion!)
* An array of six scores is rolled, using 4d6 drop low (roll four six-sided dice and take the sum of the highest three.)
* Your scores are sorted according to how important they are for your class. For instance, a Wizard needs high Intelligence and doesn't really need Strength, so a Wizard character has their highest score put into Intelligence and their lowest put into Strength, with the rest somewhere in the middle. Yes, I made the score rankings for each class; yes, they're somewhat arbitrary. I may revisit them. If you want to play a super buff wizard, please take liberties accordingly.
* _After_ the scores are sorted, bonuses for your character's Race (e.g. extra Constitution for a Dwarf) are applied. So, if you get a Dwarf with a CON score of 19, that means you rolled a 17 on your "dice" and got an extra 2 points of Constitution just for being a Dwarf. Keep this in mind if you decide to shuffle your scores around and play that super buff wizard.

This is a work in progress. Things I may add include:
* Higher level character options, in case you need a replacement character or are joining a high level party as a newcomer.
* Random spell selection for casters. This could produce some strange results, but then again, that's the stuff good roleplay is made of.
* Clean and pretty character sheet output.
* Standard array and point buy options, to let you make a custom but street-legal character per Player Handbook rules.

Have fun, and may all your hits be crits! Any ideas, comments, or suggestions - I'd love to hear them! Thanks as always to [antalsz](https://gitlab.com/antalsz) for rubberducking help.
